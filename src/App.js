import { useState } from "react";
import { TodoList } from "./components/TodoList";
import { InputForm } from "./components/Input";

function App() {
  const [todos, setTodos] = useState([
    {
      id: 1,
      title: "Artash",
      userId: 69,
      completed: false,
    },
  ]);

  function deleteTodo(id) {
    setTodos((prevTodos) => {
      return prevTodos.filter((todo) => {
        return todo.id !== id;
      });
    });
  }

  function addTodo(newTodo) {
    setTodos((prevTodos) => {
      return [...prevTodos, newTodo];
    });
  }

  function toggleTodoCompleted(id) {
    setTodos((todos) => {
      return todos.map((todo) => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed,
          };
        }
        return todo;
      });
    });
  }

  return (
    <div className="App">
      <InputForm addTodo={addTodo} />
      <TodoList
        deleteTodo={deleteTodo}
        toggleTodoCompleted={toggleTodoCompleted}
        todos={todos}
      />
      <pre>{JSON.stringify(todos, null, 2)}</pre>
    </div>
  );
}

export default App;
