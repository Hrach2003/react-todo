import { useState } from "react";

export function InputForm(props) {
  const [value, setValue] = useState("");
  function handleValueChange(event) {
    setValue(event.target.value);
  }

  function handleOnClick(event) {
    const newTodo = {
      id: Math.random(),
      title: value,
      completed: false,
      userId: Math.random(),
    };
    props.addTodo(newTodo);
    setValue("");
  }

  return (
    <div
      style={{
        width: 500,
        height: 40,
        margin: "10px auto",
        display: "flex",
        justifyContent: "space-between",
      }}
    >
      <input
        value={value}
        style={{ height: "100%", marginRight: 5, fontSize: 20 }}
        type="text"
        onChange={handleValueChange}
      />
      <button onClick={handleOnClick} style={{ height: "100%" }}>
        Add new todo
      </button>
    </div>
  );
}
