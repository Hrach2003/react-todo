import { TodoItem } from "./TodoItem";

export function TodoList(props) {
  return (
    <div
      style={{
        width: 400,
        margin: "0 auto",
      }}
    >
      <div style={{ margin: 5 }}>Todo list</div>
      {props.todos.map((todo) => {
        return (
          <TodoItem
            deleteTodo={props.deleteTodo}
            onChange={props.toggleTodoCompleted}
            key={todo.id}
            todo={todo}
          />
        );
      })}
    </div>
  );
}
