export function TodoItem(props) {
  return (
    <div
      style={{ display: "flex", height: 40, justifyContent: "space-between" }}
    >
      <input
        type="checkbox"
        onChange={() => props.onChange(props.todo.id)}
        checked={props.todo.completed}
      />
      <div>{props.todo.title}</div>
      <button onClick={() => props.deleteTodo(props.todo.id)}>X</button>
    </div>
  );
}
